/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_app_beacon_main main.c
 * @{
 * @ingroup ble_sdk_app_beacon
 * @brief Beacon Transmitter Sample Application main file.
 *
 * This file contains the source code for an Beacon transmitter sample application.
 */

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "ble_advdata.h"
#include "nordic_common.h"
//#include "pstorage.h"
#include "softdevice_handler.h"
#include "bsp.h"
#include "app_timer.h"

// Trying to include  Low Power Comparator library
#include "nrf_drv_lpcomp.h"

// Trying to include GPIO library
#include "nrf_gpio.h"

// Define for modifications on bluetooth stack data
#define MOD

// Define for PINS
#define ENABLE_PIN 	8
#define LED_PIN		6
#define RESET_PIN	10

#define CENTRAL_LINK_COUNT              0                                 /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           0                                 /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define IS_SRVC_CHANGED_CHARACT_PRESENT 0                                 /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define APP_CFG_NON_CONN_ADV_TIMEOUT    0                                 /**< Time for which the device must be advertising in non-connectable mode (in seconds). 0 disables timeout. */
#define NON_CONNECTABLE_ADV_INTERVAL    MSEC_TO_UNITS(100, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */

#define APP_BEACON_INFO_LENGTH          0x17                              /**< Total length of information advertised by the Beacon. */
#define APP_ADV_DATA_LENGTH             0x15                              /**< Length of manufacturer specific data in the advertisement. */
#define APP_DEVICE_TYPE                 0x02                              /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xC3                              /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#define APP_COMPANY_IDENTIFIER          0x0059                            /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#define APP_MAJOR_VALUE                 0x01, 0x02                        /**< Major value used to identify Beacons. */ 
#define APP_MINOR_VALUE                 0x03, 0x04                        /**< Minor value used to identify Beacons. */ 
#define APP_BEACON_UUID                 0x01, 0x12, 0x23, 0x34, \
                                        0x45, 0x56, 0x67, 0x78, \
                                        0x89, 0x9a, 0xab, 0xbc, \
                                        0xcd, 0xde, 0xef, 0xf1            /**< Proprietary UUID for Beacon. */

#define DEAD_BEEF                       0xDEADBEEF                        /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define APP_TIMER_PRESCALER             0                                 /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         4                                 /**< Size of timer operation queues. */

#if defined(USE_UICR_FOR_MAJ_MIN_VALUES)
#define MAJ_VAL_OFFSET_IN_BEACON_INFO   18                                /**< Position of the MSB of the Major Value in m_beacon_info array. */
#define UICR_ADDRESS                    0x10001080                        /**< Address of the UICR register used by this example. The major and minor versions to be encoded into the advertising data will be picked up from this location. */
#endif

static ble_gap_adv_params_t m_adv_params;                                 /**< Parameters to be passed to the stack when starting advertising. */

#ifndef MOD
static uint8_t m_beacon_info[APP_BEACON_INFO_LENGTH] =                    /**< Information advertised by the Beacon. */
{
    APP_DEVICE_TYPE,     // Manufacturer specific information. Specifies the device type in this 
                         // implementation. 
    APP_ADV_DATA_LENGTH, // Manufacturer specific information. Specifies the length of the 
                         // manufacturer specific data in this implementation.
    APP_BEACON_UUID,     // 128 bit UUID value. 
    APP_MAJOR_VALUE,     // Major arbitrary value that can be used to distinguish between Beacons. 
    APP_MINOR_VALUE,     // Minor arbitrary value that can be used to distinguish between Beacons. 
    APP_MEASURED_RSSI    // Manufacturer specific information. The Beacon's measured TX power in 
                         // this implementation. 
};
#else
	uint8_t 	data[30];	// Ble stack data for advertising
    
	uint32_t	pg_size;
    uint32_t	pg_num; 	// Use last page in flash
	uint32_t *	pHidroData;
	
	//pstorage_handle_t	m_p_example_id;
#endif

APP_TIMER_DEF(tSampling);
APP_TIMER_DEF(tDecay);

uint8_t fLPCOMP = 0;


/**
 * @brief Prototype of functions
 */
static void lpcomp_event_handler(nrf_lpcomp_event_t event);
static void lpcomp_init(void);
void decayHandler();
void samplingHandler();
void updateBLEpayload(uint16_t value);
void gpio_init(void);
void timer_init(void);
uint32_t readFlashMemory(uint32_t address);
void writeFlashMemory(uint32_t address,uint32_t value);


/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in]   line_num   Line number of the failing ASSERT call.
 * @param[in]   file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(void)
{
#ifdef MOD
	uint8_t temp_data[30];
#else		
    uint32_t      err_code;
    ble_advdata_t advdata;
    uint8_t       flags = BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED;

    ble_advdata_manuf_data_t manuf_specific_data;

	manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;

#endif

#if defined(USE_UICR_FOR_MAJ_MIN_VALUES)
    // If USE_UICR_FOR_MAJ_MIN_VALUES is defined, the major and minor values will be read from the
    // UICR instead of using the default values. The major and minor values obtained from the UICR
    // are encoded into advertising data in big endian order (MSB First).
    // To set the UICR used by this example to a desired value, write to the address 0x10001080
    // using the nrfjprog tool. The command to be used is as follows.
    // nrfjprog --snr <Segger-chip-Serial-Number> --memwr 0x10001080 --val <your major/minor value>
    // For example, for a major value and minor value of 0xabcd and 0x0102 respectively, the
    // the following command should be used.
    // nrfjprog --snr <Segger-chip-Serial-Number> --memwr 0x10001080 --val 0xabcd0102
    uint16_t major_value = ((*(uint32_t *)UICR_ADDRESS) & 0xFFFF0000) >> 16;
    uint16_t minor_value = ((*(uint32_t *)UICR_ADDRESS) & 0x0000FFFF);

    uint8_t index = MAJ_VAL_OFFSET_IN_BEACON_INFO;

    m_beacon_info[index++] = MSB_16(major_value);
    m_beacon_info[index++] = LSB_16(major_value);

    m_beacon_info[index++] = MSB_16(minor_value);
    m_beacon_info[index++] = LSB_16(minor_value);
#endif

#ifndef MOD	
    manuf_specific_data.data.p_data = (uint8_t *) m_beacon_info;
    manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;  
	
	// Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type             = BLE_ADVDATA_NO_NAME;
    advdata.flags                 = flags;
    advdata.p_manuf_specific_data = &manuf_specific_data;

    err_code = ble_advdata_set(&advdata, NULL);
    APP_ERROR_CHECK(err_code);
#else
	memset(data, '\0', sizeof(data));

	data[0] = 18;
	data[1] = 0x09;//BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA;
	
	memset(temp_data,'\0',sizeof(temp_data));
	temp_data[1] = 0x09;
	strcpy((char *) temp_data+2,"IOT");
	temp_data[0] = strlen((char *) temp_data+1);

	strcpy((char *)data,(char *)temp_data);


	data[0] = 15; 		// Lenght (type + data)
	data[1] = 0x09; 	// Type of data (0x09: Complete Name)
	data[2] = 'H';
	data[3] = 'i';
	data[4] = 'd';
	data[5] = 'r';
	data[6] = 'o';
	data[7] = 'm';
	data[8] = 'e';
	data[9] = 't'; 
	data[10] = 'r';
	data[11] = 'o';
	data[12] = ' ';
	data[13] = 'I';
	data[14] = 'O';
	data[15] = 'T';
	data[16] = 5; 		// Lenght (type + data)
	data[17] = 0xff; 	// Type of data (0xff: Manufacture company data)
	data[18] = 0;
	data[19] = 0;
	data[20] = 0;
	data[21] = 0;

	sd_ble_gap_adv_data_set(data,30,NULL,0);
#endif

    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    m_adv_params.p_peer_addr = NULL;                             // Undirected advertisement.
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = NON_CONNECTABLE_ADV_INTERVAL;
    m_adv_params.timeout     = APP_CFG_NON_CONN_ADV_TIMEOUT;

	// <<<-------------------------------------------------------------------------
	sd_ble_gap_tx_power_set(4);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_start(&m_adv_params);
    APP_ERROR_CHECK(err_code);

    //err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
    //APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;
    
    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;
    
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for doing power management.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

/*****************************************************************************/

#ifdef MOD

/** @brief Function for filling a page in flash with a value.
 *
 * @param[in] address Address of the first word in the page to be filled.
 * @param[in] value Value to be written to flash.
 */

/*
static void flash_word_write(uint32_t * address, uint32_t value)
{
    // Turn on flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    *address = value;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash write enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}
*/

/** @brief Function for erasing a page in flash.
 *
 * @param page_address Address of the first word in the page to be erased.
 */
/*
static void flash_page_erase(uint32_t * page_address)
{
    // Turn on flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Een << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Erase page:
    NRF_NVMC->ERASEPAGE = (uint32_t)page_address;

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }

    // Turn off flash erase enable and wait until the NVMC is ready:
    NRF_NVMC->CONFIG = (NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos);

    while (NRF_NVMC->READY == NVMC_READY_READY_Busy)
    {
        // Do nothing.
    }
}

*/

/*

static void example_cb_handler(pstorage_handle_t *	handle,
								uint8_t				op_code,
								uint32_t			result,
								uint8_t			*	p_data,
								uint32_t			data_len)
{
}

static void pstorage_local_init(void)
{
	pstorage_module_param_t 	p_example_param;

	p_example_param.block_size	= 0x04;
	p_example_param.block_count	= 0;
	p_example_param.cb			= example_cb_handler;

	pstorage_init();

	pstorage_register(&p_example_param,&m_p_example_id);
}

static void pstorage_local_write(uint8_t info)
{
	pstorage_handle_t p_block_id;
	
	pstorage_block_identifier_get(&m_p_example_id,0,&p_block_id);
	pstorage_store(&p_block_id, &info, 1, 0);
}

*/

/**
 * @brief Function for lpcomp interrupt event
 */
static void lpcomp_event_handler(nrf_lpcomp_event_t event)
{
	//uint16_t 	temp_data = ((data[20]<<8) | data[21]);

	fLPCOMP = 1;

	//updateBLEpayload(++temp_data);

	//temp_data++;

	//pstorage_local_write(temp_data);
	
	//data[21] = temp_data;
	
	//sd_ble_gap_adv_data_set(data,30,NULL,0);
}

/**
 * Function for lpcomp init
 */
static void lpcomp_init(void)
{
	nrf_drv_lpcomp_init(NULL, lpcomp_event_handler);
	nrf_drv_lpcomp_enable();
}
#endif

/**
 * Function for timer sampling event handling
 */
void decayHandler()
{
	nrf_gpio_pin_toggle(LED_PIN);
	nrf_gpio_pin_toggle(RESET_PIN);
	nrf_gpio_pin_toggle(ENABLE_PIN);
	
	nrf_drv_lpcomp_enable();

	app_timer_start(tSampling,5,NULL);
}

/**
 * @brief Fuction for update BLE advertising packet
 */
void updateBLEpayload(uint16_t value)
{
	data[20] = (uint8_t) ((value>>8) & 0xFF);
	data[21] = (uint8_t) (value & 0xFF); 
	sd_ble_gap_adv_data_set(data,30,NULL,0);
}

/**
 * Function for settling time event handling
 */
void samplingHandler()
{
	enum {PRE_METALLIC,METALLIC,NON_METALLIC1,NON_METALLIC2,NON_METALLIC3};
	static uint8_t state = 0;
	uint16_t measure = 0;
	
	nrf_gpio_pin_toggle(LED_PIN);
	nrf_gpio_pin_toggle(RESET_PIN);
	nrf_gpio_pin_toggle(ENABLE_PIN);
	
	nrf_drv_lpcomp_disable();
	
	app_timer_stop(tSampling);

	switch(state)
	{
		case PRE_METALLIC:
			if (fLPCOMP == 1)
				state = METALLIC;
			else if (fLPCOMP == 0)
				state = PRE_METALLIC;
		break;

		case METALLIC:
			if (fLPCOMP == 1)
				state = METALLIC;
			else if (fLPCOMP == 0)
				state = NON_METALLIC1;
		break;

		case NON_METALLIC1:
			if (fLPCOMP == 0)
				state = NON_METALLIC2;
			else if (fLPCOMP == 1)
				state = METALLIC;
		break;
		
		case NON_METALLIC2:
			if (fLPCOMP == 0)
				state = NON_METALLIC3;
			else if (fLPCOMP == 1)
				state = METALLIC;
		break;

		case NON_METALLIC3:
			if (fLPCOMP == 1)
			{
				state = METALLIC;
			}
			else if (fLPCOMP == 0)
			{
				//measure = readFlashMemory(MEASURE_ADDRESS);
				//measure++;
				//writeFlashMemory(MEASURE_ADDRESS,measure);
				measure = ((data[20]<<8) | data[21]);
				measure++;

				updateBLEpayload((uint16_t) measure);
				
				state = PRE_METALLIC;
			}
		break;
	}
	
	fLPCOMP = 0;	
}

/**
 * Function for GPIO init
 */
void gpio_init(void)
{
	nrf_gpio_cfg_output(RESET_PIN);
	nrf_gpio_cfg_output(ENABLE_PIN);
	nrf_gpio_cfg_output(LED_PIN);
	nrf_gpio_pin_clear(RESET_PIN);
	nrf_gpio_pin_clear(ENABLE_PIN);
	nrf_gpio_pin_clear(LED_PIN);

	nrf_gpio_pin_toggle(RESET_PIN);
}

/**
 * Function for timers init
 */
void timer_init(void)
{
	// Timer tick configuration. Tick = 32768/prescaler = 30.5 us (prescaler = 0)
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
	
	// Create decay timer 
	app_timer_create(&tDecay,APP_TIMER_MODE_REPEATED,decayHandler);
	app_timer_start(tDecay,32700,NULL);

	// Create sampling timer
	app_timer_create(&tSampling,APP_TIMER_MODE_REPEATED,samplingHandler);
}

/**
 * @brief Function for reading flash memory
 */
uint32_t readFlashMemory(uint32_t address)
{
	return (uint32_t) NULL;
}

/**
 * @brief Function for writing flash memory
 */
void writeFlashMemory(uint32_t address,uint32_t value)
{
}

/**
 * @brief Function for application main entry.
 */
int main(void)
{
	// Read Flash Content of Measurement
	//readFlashMemory();
	
	// Start BLE service
    ble_stack_init();
    advertising_init();

    // Start execution.
    advertising_start();

	// Start timers
	timer_init();

	// Start Low Power Comparator
	lpcomp_init();
 
	// Start GPIO pin
	gpio_init();

	// Enter main loop.
    for (;; )
    {
        power_manage();
    }
}


/**
 * @}
 */
