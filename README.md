# Projetos com NRF51822

Os projetos presentes neste repositório necessitam ser inseridos dentro dos exemplos fornecidos pela NORDIC. Ou seja, devem ser incluidos dentro do sdk, mais especificamente sdk11. Também é necessário fazer o download do compilador para arm com a versão correta, 5.4.1.

## Pré-requisitos

Reiterando, os seguintes pré-requisitos são necessários para se compilar os projetos

1) gcc arm none eabi 5.4.1
2) Nordic SDK 11

## Compilação

Ao compilar, atentar ao caminho do compilador arm, descrito no arquivo Makefile.posix. Se alguma biblioteca for adicionada, é necessário incluí-la no arquivo Makefile, dentro do projeto exemplo. Para gravação, o arquivo openocd.cfg deve ser utilizado.
